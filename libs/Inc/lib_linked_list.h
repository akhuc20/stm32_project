#include <stdint.h>
#include <stdlib.h>

struct list_node_t {
	void* data;
	struct list_node_t* next;
};

struct linked_list_t {
	struct list_node_t* head;
	struct list_node_t* tail;
	uint32_t size;
};

typedef struct linked_list_t ll_t;

void lib_ll_init(ll_t* ll);
void lib_ll_deinit(ll_t* ll);
uint32_t lib_ll_size(ll_t* ll);
void lib_ll_add_element(ll_t* ll, void* data);
void lib_ll_insert_element(ll_t* ll, uint32_t idx, void* data);
void lib_ll_update_element_at(ll_t* ll, uint32_t idx, void* data);
void lib_ll_remove_element_at(ll_t* ll, uint32_t idx);
void* lib_ll_element_at(ll_t* ll, uint32_t idx);
