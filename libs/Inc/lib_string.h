#ifndef __LIB_STRING_H_
#define __LIB_STRING_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

typedef char* string;

uint32_t lib_string_length(const char* str);

uint32_t lib_string_char_index(const char* str, char ch);

void lib_string_copy(char *dest, const char *src);

void lib_string_copy_max(char *dest, const char *src, uint32_t max_size);

void lib_string_copy_until_char(char *dest, const char *src, uint8_t delim);

char* lib_string_dup(const char *src);

void lib_string_free(char *src);

uint8_t lib_string_cmp(const char *str1, const char *str2);

uint8_t lib_string_cmp_max(const char *str1, const char *str2, uint32_t max_size);

void lib_string_append_str(char* str1, const char* str2);

void lib_string_append_char(char* str1, char ch);

void lib_string_set(char* str, uint8_t x, uint32_t size);

uint8_t lib_string_is_number(const char *str);

uint32_t lib_string_to_uint32(const char *str);

#ifdef __cplusplus
 }
#endif

#endif /* __LIB_STRING_H_ */
