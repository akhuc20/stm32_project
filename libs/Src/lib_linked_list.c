#include "lib_linked_list.h"

static struct list_node_t* lib_ll_node_at(ll_t* ll, uint32_t idx);

void lib_ll_init(ll_t* ll) {
	ll->head = 0;
	ll->tail = 0;
	ll->size = 0;
}

void lib_ll_deinit(ll_t* ll) {
	struct list_node_t* node_ptr = ll->head;
	struct list_node_t* next_node_ptr;
	while(node_ptr) {
		next_node_ptr = node_ptr->next;
		free(node_ptr);
		node_ptr = next_node_ptr;
	}
	ll->head = 0;
	ll->tail = 0;
	ll->size = 0;
}

uint32_t lib_ll_size(ll_t* ll) {
	return ll->size;
}

void lib_ll_add_element(ll_t* ll, void* data) {
	if(ll->size == 0) {
		ll->head = malloc(sizeof(struct list_node_t));
		ll->head->data = data;
		ll->head->next = 0;
		ll->tail = ll->head;
	} else {
		ll->tail->next = malloc(sizeof(struct list_node_t));
		ll->tail = ll->tail->next;
		ll->tail->data = data;
		ll->tail->next = 0;
	}
	ll->size++;
}

void lib_ll_insert_element(ll_t* ll, uint32_t idx, void* data) {
	if(ll == 0) return;
	if(idx > ll->size) return;

	if(idx == ll->size) {
		lib_ll_add_element(ll, data);
		return;
	}
	struct list_node_t *node_ptr, *next_node_ptr;

	if(idx == 0) {
		node_ptr = ll->head;
		ll->head = malloc(sizeof(struct list_node_t));
		ll->head->data = data;
		ll->head->next = node_ptr;
		ll->size++;
		return;
	}
	node_ptr = lib_ll_node_at(ll, idx-1);
	next_node_ptr = node_ptr -> next;
	node_ptr -> next = malloc(sizeof(struct list_node_t));
	node_ptr -> next -> data = data;
	node_ptr -> next -> next = next_node_ptr;
	ll->size++;
}

void lib_ll_update_element_at(ll_t* ll, uint32_t idx, void* data) {
	if(ll == 0) return;
	if(idx >= ll->size) return;
	lib_ll_node_at(ll, idx) -> data = data;
}

void lib_ll_remove_element_at(ll_t* ll, uint32_t idx) {
	if(ll == 0) return;
	if(idx >= ll->size) return;
	struct list_node_t *node_ptr, *next_node_ptr;
	if(idx == 0) {
		node_ptr =  ll -> head -> next;
		free(ll -> head);
		ll -> head = node_ptr;
		return;
	}
	node_ptr = lib_ll_node_at(ll, idx-1);
	next_node_ptr = node_ptr -> next -> next;
	free(node_ptr -> next);
	node_ptr -> next = next_node_ptr;
	if(idx == ll->size - 1) {
		ll->tail = node_ptr;
	}
	ll -> size--;
}

void* lib_ll_element_at(ll_t* ll, uint32_t idx) {
	if(ll == 0) return 0;
	if(idx >= ll->size) return 0;
	struct list_node_t* node_ptr = ll-> head;
	while(idx--) {
		node_ptr = node_ptr -> next;
	}
	return node_ptr->data;
}

static struct list_node_t* lib_ll_node_at(ll_t* ll, uint32_t idx) {
	if(ll == 0) return 0;
	if(idx >= ll->size) return 0;
	struct list_node_t* node_ptr = ll-> head;
	while(idx--) {
		node_ptr = node_ptr -> next;
	}
	return node_ptr;
}
