
#include "lib_string.h"

uint32_t lib_string_length(const char* str) {
	uint32_t res = 0;
	for(; str[res]; res++);
	return res;
}

uint32_t lib_string_char_index(const char* str, char ch) {
	uint32_t i;
	for(i = 0; str[i]; i++) {
		if(str[i] == ch) break;
	}
	return i;
}

void lib_string_copy(char *dest, const char *src) {
	for(; *src; src++, dest++) {
		*dest = *src;
	}
}

void lib_string_copy_max(char *dest, const char *src, uint32_t max_size) {
	for(uint32_t i = 0; i < max_size && src[i]; i++) {
		dest[i] = src[i];
	}
}

void lib_string_copy_until_char(char *dest, const char *src, uint8_t delim) {
	for(; *src && *src != delim; src++, dest++) {
		*dest = *src;
	}
}

// returned address must be freed by user
char* lib_string_dup(const char *src) {
	uint32_t size = lib_string_length(src);
	char *res = malloc(size + 1);
	lib_string_set(res, 0, size+1);
	lib_string_copy(res, src);
	return res;
}

void lib_string_free(char *src) {
	free(src);
}

uint8_t lib_string_cmp(const char *str1, const char *str2) {
	for(; *str1 && *str2; str1++, str2++) {
		if(*str1 != *str2) return -1;
	}
	return (uint8_t) *str1 - (uint8_t) *str2;	// if both of them is '\0' 0 is returned, non-zero otherwise
}

uint8_t lib_string_cmp_max(const char *str1, const char *str2, uint32_t max_size) {
	for(; *str1 && *str2 && max_size; str1++, str2++, max_size--) {
		if(*str1 != *str2)
			return (uint8_t) *str1 - (uint8_t) *str2;
	}
	return max_size * ((uint8_t) *str1 - (uint8_t) *str2);  // if max_size has reached 0, than 0 should be returned, simple comparison otherwise
}

void lib_string_append_str(char* str1, const char* str2) {
	lib_string_copy(str1 + lib_string_length(str1), str2);
}

void lib_string_append_char(char* str1, char ch) {
	*(str1 + lib_string_length(str1)) = ch;
}

void lib_string_set(char* str, uint8_t x, uint32_t size) {
	while(size--) {
		str[size] = x;
	}
}

uint8_t lib_string_is_number(const char *str) {
	for(; *str; str++) {
		if(*str < '0' || *str > '9') return 0;
	}
	return 1;
}

uint32_t lib_string_to_uint32(const char *str) {
	uint32_t res = 0;
	for(; *str; str++) {
		res *= 10;
		res += (*str - '0');
	}
	return res;
}

