
#ifndef __OS_TASKS_H_
#define __OS_TASKS_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "lib_string.h"
#include "mw_uart.h"
#include "os_task_idxs.h"
#include "mw_command_idxs.h"

#define COMMAND_MAX_SIZE (512)
#define MAX_ARGS_NUM (0xff)

#define COMMANDS_NUM 5

#define sizeof_arr(arr) sizeof(arr) / sizeof((arr)[0])

typedef void (*task_fn_t)(void*);

struct os_task_t {
	string name;
	TaskHandle_t *handle_ptr;
	task_fn_t fn;
	uint32_t stack_size_additional;	// how many bytes will its stack need except configMINIMAL_STACK_SIZE
	uint32_t priority;
	uint8_t is_killable;
	uint8_t is_running;
};

struct command_t {
	string command;
	string desc;
	enum {
		NoArgs,				// ex.: ls
		DistinctArgs,		// ex.: set PORTB 14 1
		OneBigArg			// ex.: echo
	} arg_usage;
	uint16_t task_idx;
};

struct arg_list_t {
	uint8_t argc;
	string * argv;
};

struct pin_task_t {
	GPIO_TypeDef* port;
	uint16_t pin;
	TaskHandle_t *handle_ptr;
	uint32_t timeout1;
	uint32_t timeout2;
	uint8_t user_pin_state;
	uint8_t is_running;
};

extern struct os_task_t tasks[];
extern struct command_t commands[];

void os_task_mgr(void* data);
void os_task_cli(void* data);
void os_task_echo(void* data);
void os_task_help(void* data);
void os_task_port(void* data);

void os_task_manager_init();
struct os_task_t* get_task_by_name(string name);
void os_start_task(struct os_task_t* tsk);
void os_close_task(struct os_task_t* tsk);

void free_arg_list(struct arg_list_t * list);



#ifdef __cplusplus
 }
#endif

#endif /* __OS_TASKS_H_ */
