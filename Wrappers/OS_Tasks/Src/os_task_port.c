#include "os_tasks.h"

#define PORTA_N 0
#define PORTB_N 1
#define PORTC_N 2
#define PORTD_N 3
#define PORTE_N 4
#define CHECK_SHIFT 4

#define pin_to_bitmask(PORT_N, PIN_IDX) ((PORT_N << CHECK_SHIFT) | PIN_IDX)

static void init_all_ports();
static void init_all_pins_in_port(GPIO_TypeDef* GPIOx);
static uint8_t is_available(uint8_t pin);
static void run_set(struct arg_list_t* args);
static void clear_all();
static uint8_t string_to_portn(string str);
static void close_pin_task(struct pin_task_t* pin_task);

static void set_timeout_fn(void* data);
static void set_cycle_fn(void* data);

uint8_t unavailable_pins[] = {pin_to_bitmask(PORTA_N, 0),		// PA0	 used for WKUP
							  pin_to_bitmask(PORTA_N, 9),		// PA9	 used for VBUS of CN5 (on our board)
							  pin_to_bitmask(PORTA_N, 11),		// PA11  used for USB
							  pin_to_bitmask(PORTA_N, 12),		// PA12	 used for USB
							  pin_to_bitmask(PORTA_N, 13),		// PA13  used for (JTMS-SWDIO)
							  pin_to_bitmask(PORTA_N, 14),		// PA14  used for (JTCK/SWCLK)
							  pin_to_bitmask(PORTA_N, 15),		// PA15  used for (JTDI)

							  pin_to_bitmask(PORTB_N, 2),		// PB2	 used for BOOT1
							  pin_to_bitmask(PORTB_N, 3),		// PB3   used for (JTDO/TRACESWO)
							  pin_to_bitmask(PORTB_N, 4),		// PB4   used for (NJTRST)
							  pin_to_bitmask(PORTB_N, 6),		// PB6 	 used for UART
							  pin_to_bitmask(PORTB_N, 7),		// PB7 	 used for UART
							 };

struct pin_task_t pin_tasks[16 * 5] = {0};

void os_task_port(void* data) {
	struct arg_list_t* args;
	init_all_ports();					// initially all pins of all ports are initialized as output pins (except unavailable pins)
	while(1) {
		xTaskNotifyWait(0, 0, (uint32_t *)&args, portMAX_DELAY);
		if(args == 0) break;
		if(lib_string_cmp(args->argv[0], "set") == 0) {
			run_set(args);
		}
		free_arg_list(args);
	}

	clear_all();
	vTaskDelete(0);
}

static void init_all_ports() {
	init_all_pins_in_port(GPIOA);
	init_all_pins_in_port(GPIOB);
	init_all_pins_in_port(GPIOC);
	init_all_pins_in_port(GPIOD);
	init_all_pins_in_port(GPIOE);
}

static void init_all_pins_in_port(GPIO_TypeDef* GPIOx) {
	uint8_t portn;
	uint32_t pin_mask = 0;
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	switch((uint32_t)GPIOx) {
		case (uint32_t)GPIOA:
			portn = PORTA_N;
			break;
		case (uint32_t)GPIOB:
			portn = PORTB_N;
			break;
		case (uint32_t)GPIOC:
			portn = PORTC_N;
			break;
		case (uint32_t)GPIOD:
			portn = PORTD_N;
			break;
		case (uint32_t)GPIOE:
			portn = PORTE_N;
			break;
		default: return;
	}
	for(uint8_t i = 0; i < 16; i++) {
		if(is_available(pin_to_bitmask(portn, i))) {
			pin_mask |= (1UL << i);
			pin_tasks[portn*16 + i].handle_ptr = malloc(sizeof(TaskHandle_t));
			pin_tasks[portn*16 + i].pin = (1UL << i);
			pin_tasks[portn*16 + i].port = GPIOx;
			pin_tasks[portn*16 + i].is_running = 0;
		}
	}

	HAL_GPIO_WritePin(GPIOx, pin_mask, GPIO_PIN_RESET);
	GPIO_InitStruct.Pin = pin_mask;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}

static uint8_t is_available(uint8_t pin) {
	for(uint8_t i = 0; i < sizeof(unavailable_pins)/sizeof(unavailable_pins[0]); i++) {
		if(pin == unavailable_pins[i]) return 0;
	}
	return 1;
}

// command: set PORT PIN VAL ["timeout" duration_in_ms [toggle]]
// command: set PORT PIN VAL ["cycle" duration1_in_ms [duration2_in_ms]]
static void run_set(struct arg_list_t* args) {
	if(args->argc < 4) {
		mw_uart_send_str("weirdo...\r\n");
		return;
	}

	uint8_t portn = string_to_portn(args->argv[1]);
	if(portn == -1) {
		mw_uart_send_str("unknown port specified\r\n");
		return;
	}
	uint8_t pin = (uint8_t)lib_string_to_uint32(args->argv[2]);
	if(pin == -1) {
		mw_uart_send_str("weird pin but ok...\r\n");
		return;
	}

	if(!is_available(pin_to_bitmask(portn, pin))) {
		mw_uart_send_str("The specified pin is not really general purpose...\r\n");
		return;
	}
	uint8_t state = lib_string_to_uint32(args->argv[3]);
	if(state != 0 && state != 1) {
		mw_uart_send_str("this is digital, man, 1 or 0!!!\r\n");
		return;
	}

	struct pin_task_t* cur_pin_task = &pin_tasks[portn*16 + pin];
	if(cur_pin_task->is_running) {
		close_pin_task(cur_pin_task);
	}
	if(args->argc == 4) {
		HAL_GPIO_WritePin(cur_pin_task->port, cur_pin_task->pin, state);
	} else if(args->argc < 6 || args->argc > 7) {
		mw_uart_send_str("weird number of arguments...\r\n");
		return;
	} else {
		void (*fun)(void*);
		if(lib_string_cmp(args->argv[4], "timeout") == 0) {
			fun = set_timeout_fn;
		} else if(lib_string_cmp(args->argv[4], "cycle") == 0) {
			fun = set_cycle_fn;
		} else {
			mw_uart_send_str("the fourth argument must be \"timeout\" or \"cycle\"!\r\n");
			return;
		}
		cur_pin_task->timeout1 = lib_string_to_uint32(args->argv[5]);
		if(args->argc == 7) {
			cur_pin_task->timeout2 = lib_string_to_uint32(args->argv[6]);
		} else {
			cur_pin_task->timeout2 = cur_pin_task->timeout1;
		}
		cur_pin_task->user_pin_state = state;
		cur_pin_task->is_running = 1;
		xTaskCreate(fun,
					"",
					configMINIMAL_STACK_SIZE + 8,
					cur_pin_task,
					1,
					cur_pin_task->handle_ptr);
	}

}

static void close_pin_task(struct pin_task_t* pin_task) {
	xTaskNotify(*pin_task->handle_ptr, 0, eSetBits);
	pin_task->is_running = 0;
}

static void set_timeout_fn(void* data) {
	struct pin_task_t * cur_task = data;
	HAL_GPIO_WritePin(cur_task->port, cur_task->pin, cur_task->user_pin_state);
	vTaskDelay(cur_task -> timeout1);
	xTaskNotifyWait(0, 0, 0, cur_task -> timeout1);
	HAL_GPIO_TogglePin(cur_task->port, cur_task->pin);
	cur_task->is_running = 0;
	vTaskDelete(0);
}

static void set_cycle_fn(void* data) {
	struct pin_task_t * cur_task = data;
	uint8_t odd = 1;
	HAL_GPIO_WritePin(cur_task->port, cur_task->pin, cur_task->user_pin_state);
	while(cur_task->is_running) {
		if(odd) {
			HAL_GPIO_WritePin(cur_task->port, cur_task->pin, cur_task->user_pin_state);
			xTaskNotifyWait(0, 0, 0, cur_task -> timeout1);
		} else {
			HAL_GPIO_WritePin(cur_task->port, cur_task->pin, !cur_task->user_pin_state);
			xTaskNotifyWait(0, 0, 0, cur_task -> timeout2);
		}
		odd = !odd;
	}
	vTaskDelete(0);
}

static void clear_all() {
	struct pin_task_t * cur_task_ptr;
	for(uint8_t i = 0; i < sizeof(pin_tasks) / sizeof(pin_tasks[0]); i++) {
		cur_task_ptr = &pin_tasks[i];
		if(cur_task_ptr->handle_ptr != 0) {
			if(cur_task_ptr->is_running) close_pin_task(cur_task_ptr);
			free(cur_task_ptr->handle_ptr);
			HAL_GPIO_WritePin(cur_task_ptr->port, cur_task_ptr->pin, 0);
		}
	}
}

static uint8_t string_to_portn(string str) {
	char ch;
	if(lib_string_length(str) == 1) {
		ch = str[0];
	} else if(lib_string_length(str) == lib_string_length("PORTA")
				&& lib_string_cmp_max(str, "PORT", 4) == 0) {
		ch = str[lib_string_length("PORT")];
	} else {
		return -1;
	}
	if(ch < 'A' || ch > 'E')
		return -1;
	return ch - 'A';
}
