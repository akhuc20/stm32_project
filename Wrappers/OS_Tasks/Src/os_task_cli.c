#include "os_tasks.h"

extern string line;
extern uint16_t line_size;
char command_buff[COMMAND_MAX_SIZE];

static struct arg_list_t * _string_to_args(string str);
static void _run_command(struct command_t *comm);
static uint8_t _count_args(string str);

struct command_t commands[] = {{.command = "echo",
								.desc = "me patara kaci var, rasac chamdzaxeben imas amovdzaxeb, imitom ki ara, rom yvelaferi mjera, anda chemi azri ara maq,\r\naara, ase moitana cxovrebam, metyvian aseao, asea-tqva, metyvian iseao - isea...",
								.arg_usage = OneBigArg,
								.task_idx = ECHO_TASK_IDX},

							   {.command = "help",
								.desc = "gisment mashveli, vis schirdeba sashveli?",
								.arg_usage = DistinctArgs,
								.task_idx = HELP_TASK_IDX},

							   {.command = "start",
								.desc = "iwyebs ravi...",
								.arg_usage = DistinctArgs,
								.task_idx = MGR_TASK_IDX},

							   {.command = "close",
								.desc = "xuravs ravi...",
							    .arg_usage = DistinctArgs,
								.task_idx = MGR_TASK_IDX},

								{.command = "set",
								 .desc = "miset-mosete da egaa",
								 .arg_usage = DistinctArgs,
								 .task_idx = PORT_TASK_IDX},
						      };

string unknown_command_message = "unknown command\r\n";
string task_not_running_message = "the corresponding task is not running currently\r\n";


void os_task_cli(void* data) {
	uint8_t i;
	struct command_t * cur_command;
	while(1) {
		lib_string_set(line, 0, RX_BUFF_SIZE);
		lib_string_set(command_buff, 0, sizeof_arr(command_buff));
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

		lib_string_copy_until_char(command_buff, line, ' ');

		for(i = 0; i < sizeof_arr(commands); i++) {
			cur_command = &commands[i];
			if(lib_string_cmp(cur_command -> command, command_buff) == 0) {
				if(tasks[cur_command->task_idx].is_running) {
					_run_command(cur_command);
				} else {
					mw_uart_send_str(command_buff);
					mw_uart_send_data(": ", 2);
					mw_uart_send_str(task_not_running_message);
				}
				break;
			}
		}
		if(i == sizeof_arr(commands)) {
			mw_uart_send_str(command_buff);
			mw_uart_send_data(": ", 2);
			mw_uart_send_str(unknown_command_message);
		}
	}
}

static void _run_command(struct command_t *comm) {
	uint32_t notification_value = 0;
	switch (comm -> arg_usage) {
		case NoArgs:
			notification_value = 0;
			break;
		case DistinctArgs:
			notification_value = (uint32_t)_string_to_args(line);
			break;
		case OneBigArg:
			notification_value = (uint32_t)lib_string_dup(line);
			break;
		default:
			notification_value = -1;
			break;
	}
	xTaskNotify(*tasks[comm -> task_idx].handle_ptr, notification_value, eSetValueWithOverwrite);
}

uint16_t space_idx;

static struct arg_list_t* _string_to_args(string str) {
	struct arg_list_t* arg_list = malloc(sizeof(struct arg_list_t));
	arg_list->argc = _count_args(str);
	arg_list->argv = malloc(sizeof(string) * arg_list->argc);
	for(uint8_t i = 0; i < arg_list->argc; ) {
		if(*str == ' ') {
			str++;
			continue;
		}
		space_idx = lib_string_char_index(str, ' ');
		str[space_idx] = '\0';

		arg_list->argv[i] = lib_string_dup(str);
		str += space_idx + 1;
		i++;
	}
	return arg_list;
}

static uint8_t _count_args(string str) {
	uint8_t res = 0;
	while(*str) {
		space_idx = lib_string_char_index(str, ' ');
		if(space_idx) {
			res++;
			str += space_idx;
		}
		str++;
	}
	return res;
}

void free_arg_list(struct arg_list_t * list) {
	for(uint8_t i = 0; i < list->argc; i++) {
		free(list->argv[i]);
	}
	free(list->argv);
	free(list);
}




