#include "os_tasks.h"

/*
struct os_task_t {
	string name;
	TaskHandle_t *handle_ptr;
	task_fn_t fn;
	uint32_t stack_size_additional;	// how many bytes will its stack need except configMINIMAL_STACK_SIZE
	uint32_t priority;
	uint8_t is_killable;
	uint8_t is_running;
};
*/

struct os_task_t tasks[] = {{"mgr_task",  0, os_task_mgr,  16, 1, 0, 1},
							{"cli_task",  0, os_task_cli,  256, 1, 0, 1},
							{"echo_task", 0, os_task_echo, 16, 1, 1, 0},
						    {"help_task", 0, os_task_help, 0, 1, 0, 1},
						    {"port_task", 0, os_task_port, 512, 1, 1, 1},
						   };

void os_task_mgr(void* data) {
	struct arg_list_t* args;
	struct os_task_t* tsk;
	while(1) {
		xTaskNotifyWait(0, 0, (uint32_t*) &args, portMAX_DELAY);
		if(args->argc > 1) {
			tsk = get_task_by_name(args->argv[1]);
			if(tsk) {
				if(lib_string_cmp(args->argv[0], commands[START_CMD_IDX].command) == 0) {
					os_start_task(tsk);
				} else {
					os_close_task(tsk);
				}
			} else mw_uart_send_str("no such task to start/close\r\n");
		} else {
			mw_uart_send_str("specify the task you are talking about\r\n");
		}
		free_arg_list(args);
	}
	vTaskDelete(0);
}

void os_task_manager_init() {
	struct os_task_t * cur_task;
	uint16_t i;
	for(i = 0; i < sizeof_arr(tasks); i++) {
		cur_task = &tasks[i];
		cur_task -> handle_ptr = malloc(sizeof(TaskHandle_t));
		if(cur_task -> is_running) {
			xTaskCreate(cur_task -> fn,
						cur_task -> name,
						configMINIMAL_STACK_SIZE + cur_task->stack_size_additional,
						&cur_task-> is_running,
						cur_task -> priority,
						cur_task -> handle_ptr);
		}
	}
}

struct os_task_t* get_task_by_name(string name) {
	uint32_t name_len = lib_string_length(name);
	uint32_t name_len_with_task = name_len + lib_string_length("_task");
	for(uint16_t i = 0; i < sizeof_arr(tasks); i++) {
		if((lib_string_length(tasks[i].name) == name_len_with_task) && (lib_string_cmp_max(name, tasks[i].name, name_len) == 0))
			return &tasks[i];
	}
	return 0;
}

void os_start_task(struct os_task_t* tsk) {
	if(tsk->is_running != 0) {
		mw_uart_send_str(tsk->name);
		mw_uart_send_str(": task is already running\r\n");
		return;
	}
	tsk->is_running = 1;
	xTaskCreate(tsk -> fn,
				tsk -> name,
				configMINIMAL_STACK_SIZE + tsk->stack_size_additional,
				&tsk-> is_running,
				tsk -> priority,
				tsk -> handle_ptr);
	mw_uart_send_str(tsk->name);
	mw_uart_send_str(": task started\r\n");

}


void os_close_task(struct os_task_t* tsk) {
	if(! tsk->is_killable) {
		mw_uart_send_str(tsk->name);
		mw_uart_send_str(": task is not killable/closable\r\n");
		return;
	}
	if(tsk->is_running == 0) {
		mw_uart_send_str(tsk->name);
		mw_uart_send_str(": task is not running\r\n");
		return;
	}
	tsk->is_running = 0;
	xTaskNotify(*tsk->handle_ptr, 0, eSetValueWithOverwrite);
	mw_uart_send_str(tsk->name);
	mw_uart_send_str(": task closed\r\n");
}

