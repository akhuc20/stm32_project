#include "os_tasks.h"

void _print_about(struct command_t* comm);

void os_task_help(void* data) {
	struct arg_list_t* args;
	while(1) {
		xTaskNotifyWait(0, 0, (uint32_t*) &args, portMAX_DELAY);
		if(args->argc > 1) {
			for(uint16_t i = 0; i < COMMANDS_NUM; i++) {
				if(lib_string_cmp(args->argv[1], commands[i].command) == 0) {
					_print_about(&commands[i]);
					mw_uart_send_str("\r\n");
					break;
				}
			}
		} else {
			for(uint16_t i = 0; i < COMMANDS_NUM; i++) {
				_print_about(&commands[i]);
			}
			mw_uart_send_str("\r\n");
		}
		free_arg_list(args);
	}
	vTaskDelete(0);
}

void _print_about(struct command_t* comm) {
	mw_uart_send_str("\r\n");
	mw_uart_send_str(comm->command);
	mw_uart_send_str(": ");
	mw_uart_send_str(comm->desc);
	mw_uart_send_str("\r\n");
}

