#include "os_tasks.h"

void os_task_echo(void* data) {
	uint32_t txt;
	while(1) {
		xTaskNotifyWait(0, 0, &txt, portMAX_DELAY);
		if(txt == 0) break;
		mw_uart_send_str((char*) (txt + lib_string_length("echo ")));
		mw_uart_send_data("\r\n", 2);
		free((void*) txt);
	}
	vTaskDelete(0);
}
