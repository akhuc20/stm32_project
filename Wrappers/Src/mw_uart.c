#include "mw_uart.h"

static void _uart_dma_init();
static void _uart_init(uint32_t baud_rate);
static void _callback_default(uint8_t* buff, uint16_t size);
static void _wait_for_busy();
static void _release_busy();
static uint32_t _abs(uint32_t x);

uint8_t rx_buffer[RX_BUFF_SIZE];
uint8_t temp_buffer[RX_BUFF_SIZE];

DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;

UART_HandleTypeDef huart;
uint8_t uart_busy;

static void (*callback)(uint8_t* buff, uint16_t size);

/* initializes UART with baud rate equal to 'baud_rate' or 115200 if rate set to 0
 *  PB6 -> Tx
 *  PB7 -> Rx
 */
void mw_uart_init(uint32_t baud_rate) {
	_uart_dma_init();
	_uart_init(baud_rate);
	callback = _callback_default;
	HAL_UARTEx_ReceiveToIdle_DMA(&huart, rx_buffer, RX_BUFF_SIZE);
	__HAL_DMA_DISABLE_IT(&hdma_usart1_rx, DMA_IT_HT);
	uart_busy = 0;
}

void mw_uart_send_data(uint8_t *data, uint32_t n) {
	_wait_for_busy();
	vTaskDelay(10);
	HAL_UART_Transmit_DMA(&huart, data, n);
	_release_busy();
}

void mw_uart_send_byte(uint8_t *byte) {
	mw_uart_send_data(byte, 1);
}

/* transmits 0-terminated string through UART without '/0' */
void mw_uart_send_str(char *str) {
	uint32_t size = lib_string_length(str);
	mw_uart_send_data((uint8_t *)str, size);
}

void mw_uart_set_rx_event_callback(void func(uint8_t* buff, uint16_t size)) {
	callback = func;
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *handle_uart, uint16_t size) {
	lib_string_copy_max((char *)temp_buffer, (char *)rx_buffer, size);
	callback(temp_buffer, size);

	HAL_UARTEx_ReceiveToIdle_DMA(handle_uart, rx_buffer, RX_BUFF_SIZE);
	__HAL_DMA_DISABLE_IT(&hdma_usart1_rx, DMA_IT_HT);
}

static void _uart_dma_init() {
	/* DMA controller clock enable */
	__HAL_RCC_DMA2_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA2_Stream5_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream5_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream5_IRQn);
	/* DMA2_Stream7_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);
}

static uint32_t _abs(uint32_t x) {
	if(x>=0) return x;
	return -x;
}

static void _uart_init(uint32_t baud_rate) {
	huart.Instance = USART1;
	huart.Init.BaudRate = baud_rate ? _abs(baud_rate) : 115200;
	huart.Init.WordLength = UART_WORDLENGTH_8B;
	huart.Init.StopBits = UART_STOPBITS_1;
	huart.Init.Parity = UART_PARITY_NONE;
	huart.Init.Mode = UART_MODE_TX_RX;
	huart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart.Init.OverSampling = UART_OVERSAMPLING_16;

	if (HAL_UART_Init(&huart) != HAL_OK) {
		__disable_irq();
		while (1) { }
	}
}

void HAL_UART_MspInit(UART_HandleTypeDef *huart) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	/* Peripheral clock enable */
	__HAL_RCC_USART1_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	/**USART1 GPIO Configuration */
	GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/* USART1 DMA Init */
	/* USART1_RX Init */
	hdma_usart1_rx.Instance = DMA2_Stream5;
	hdma_usart1_rx.Init.Channel = DMA_CHANNEL_4;
	hdma_usart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma_usart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_usart1_rx.Init.MemInc = DMA_MINC_ENABLE;
	hdma_usart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_usart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma_usart1_rx.Init.Mode = DMA_NORMAL;
	hdma_usart1_rx.Init.Priority = DMA_PRIORITY_LOW;
	hdma_usart1_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	if (HAL_DMA_Init(&hdma_usart1_rx) != HAL_OK) {
		__disable_irq();
		while (1) {
		}
	}

	__HAL_LINKDMA(huart, hdmarx, hdma_usart1_rx);

	/* USART1_TX Init */
	hdma_usart1_tx.Instance = DMA2_Stream7;
	hdma_usart1_tx.Init.Channel = DMA_CHANNEL_4;
	hdma_usart1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
	hdma_usart1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_usart1_tx.Init.MemInc = DMA_MINC_ENABLE;
	hdma_usart1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_usart1_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma_usart1_tx.Init.Mode = DMA_NORMAL;
	hdma_usart1_tx.Init.Priority = DMA_PRIORITY_LOW;
	hdma_usart1_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	if (HAL_DMA_Init(&hdma_usart1_tx) != HAL_OK) {
		__disable_irq();
		while (1) {
		}
	}

	__HAL_LINKDMA(huart, hdmatx, hdma_usart1_tx);

	/* USART1 interrupt Init */
	HAL_NVIC_SetPriority(USART1_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(USART1_IRQn);

}

static void _callback_default(uint8_t *buff, uint16_t size) {
	HAL_UARTEx_ReceiveToIdle_DMA(&huart, rx_buffer, RX_BUFF_SIZE);
	__HAL_DMA_DISABLE_IT(&hdma_usart1_rx, DMA_IT_HT);
}

void USART1_IRQHandler() {
	HAL_UART_IRQHandler(&huart);
}

/**
 * @brief This function handles DMA2 stream5 global interrupt.
 */
void DMA2_Stream5_IRQHandler() {
	HAL_DMA_IRQHandler(&hdma_usart1_rx);
}

/**
 * @brief This function handles DMA2 stream7 global interrupt.
 */
void DMA2_Stream7_IRQHandler() {
	HAL_DMA_IRQHandler(&hdma_usart1_tx);
}

static void _wait_for_busy() {
	while(uart_busy) {}
	uart_busy = 1;
}

static void _release_busy() {
	uart_busy = 0;
}

