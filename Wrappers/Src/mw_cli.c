#include "mw_cli.h"

#define sizeof_arr(arr) sizeof(arr) / sizeof((arr)[0])

static void _cli_callback(uint8_t* buff, uint16_t size);

string line;
uint16_t line_size;

void mw_cli_init() {
	mw_uart_init(0);

	os_task_manager_init();

	mw_uart_set_rx_event_callback(_cli_callback);
}

static void _cli_callback(uint8_t* buff, uint16_t size) {
	line = (string)buff;
	line_size = size;
	vTaskNotifyGiveFromISR(*tasks[CLI_TASK_IDX].handle_ptr, 0);
}
