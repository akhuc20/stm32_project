
#ifndef __MW_UART_H_
#define __MW_UART_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include <stdint.h>
#include "stm32f4xx.h"
#include "lib_string.h"

#define STR_MAX_SIZE 1024
#define RX_BUFF_SIZE 512

// PB6 -> Tx
// PB7 -> Rx
void mw_uart_init(uint32_t rate);

void mw_uart_send_byte(uint8_t* byte);

void mw_uart_send_data(uint8_t* data, uint32_t n);

// send 0-terminated string through UART
void mw_uart_send_str(char* str);

void mw_uart_set_rx_buffer(uint8_t* buff);

void mw_uart_set_rx_event_callback(void func(uint8_t* buff, uint16_t Size));



#ifdef __cplusplus
 }
#endif

#endif /* __MW_UART_H_ */
