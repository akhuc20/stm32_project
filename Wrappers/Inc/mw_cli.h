
#ifndef __MW_CLI_H_
#define __MW_CLI_H_

#ifdef __cplusplus
 extern "C" {
#endif

#include "FreeRTOS.h"
#include "task.h"
#include "mw_uart.h"
#include "lib_string.h"
#include "os_tasks.h"
#include "mw_command_idxs.h"

void mw_cli_init();


#ifdef __cplusplus
 }
#endif

#endif /* __MW_CLI_H_ */
